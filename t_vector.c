/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_algorithm_defs.h"

#include <stdlib.h>
#include "vector.h"

#define arraycount(array)	(sizeof(array)/sizeof(array[0]))

#define INT_VECTOR_DEFAULT_CAPACITY	10
VECTOR(int_vector, int);
VECTOR_PROTOTYPE_STATIC(int_vector, int)
VECTOR_GENERATE_STATIC(int_vector, int, INT_VECTOR_DEFAULT_CAPACITY)

ATF_TC(test_int_vector);
ATF_TC_HEAD(test_int_vector, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_int_vector");
}
ATF_TC_BODY(test_int_vector, tc)
{
	int tests[4096], err, *data, value;
	struct int_vector *iv;
	size_t capacity, size, max_size, len, i, j;
	bool empty;

	len = arraycount(tests);
	for (i = 0; i < len; ++i)
		tests[i] = (int)arc4random();

	iv = VECTOR_NEW(int_vector, NULL);
	ATF_CHECK(iv != NULL);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == 0);
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity == INT_VECTOR_DEFAULT_CAPACITY);
	max_size = VECTOR_MAX_SIZE(int_vector);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == true);

	for (i = 0; i < arraycount(tests); ++i) {
		err = VECTOR_PUSH_BACK(int_vector, iv, tests[i]);
		ATF_CHECK(err == 0);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[0]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[i]);
	}

	for (i = arraycount(tests) - 1; i-- > 0;) {
		VECTOR_POP_BACK(int_vector, iv);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[0]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[i]);
	}
	VECTOR_POP_BACK(int_vector, iv);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == 0);
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity >= size);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == true);

	for (i = 0; i < arraycount(tests); ++i) {
		err = VECTOR_INSERT(int_vector, iv, 0, tests[i]);
		ATF_CHECK(err == 0);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[i - j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[i - j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[i]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[0]);
	}

	for (i = arraycount(tests) - 1; i-- > 0;) {
		VECTOR_ERASE(int_vector, iv, 0);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[i - j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[i - j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[i]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[0]);
	}
	VECTOR_ERASE(int_vector, iv, 0);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == 0);
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity >= size);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == true);

	for (i = 0; i < arraycount(tests); ++i) {
		err = VECTOR_INSERT(int_vector, iv, i, tests[i]);
		ATF_CHECK(err == 0);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[0]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[i]);
	}

	for (i = arraycount(tests) - 1; i-- > 0;) {
		VECTOR_ERASE(int_vector, iv, i + 1);
		size = VECTOR_SIZE(int_vector, iv);
		ATF_CHECK(size == i + 1);
		capacity = VECTOR_CAPACITY(int_vector, iv);
		ATF_CHECK(capacity >= size);
		ATF_CHECK(max_size >= capacity);
		empty = VECTOR_EMPTY(int_vector, iv);
		ATF_CHECK(empty == false);
		data = VECTOR_DATA(int_vector, iv);
		ATF_CHECK(data != NULL);
		for (j = 0; j <= i; ++j) {
			ATF_CHECK(data[j] == tests[j]);
			value = VECTOR_AT(int_vector, iv, j);
			ATF_CHECK(value == tests[j]);
		}
		value = VECTOR_FRONT(int_vector, iv);
		ATF_CHECK(value == tests[0]);
		value = VECTOR_BACK(int_vector, iv);
		ATF_CHECK(value == tests[i]);
	}
	VECTOR_ERASE(int_vector, iv, 0);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == 0);
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity >= size);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == true);

	for (i = 0; i < arraycount(tests); ++i)
		VECTOR_PUSH_BACK(int_vector, iv, tests[i]);
	VECTOR_CLEAR(int_vector, iv);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == 0);
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity >= size);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == true);

	for (i = 0; i < arraycount(tests); ++i)
		VECTOR_PUSH_BACK(int_vector, iv, tests[i]);
	VECTOR_SHRINK_TO_FIT(int_vector, iv);
	size = VECTOR_SIZE(int_vector, iv);
	ATF_CHECK(size == arraycount(tests));
	capacity = VECTOR_CAPACITY(int_vector, iv);
	ATF_CHECK(capacity == size);
	ATF_CHECK(max_size >= capacity);
	empty = VECTOR_EMPTY(int_vector, iv);
	ATF_CHECK(empty == false);

	VECTOR_DELETE(int_vector, iv);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_int_vector);
	return atf_no_error();
}
