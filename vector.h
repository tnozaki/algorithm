/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#ifndef ALGORITHM_VECTOR_H_
#define ALGORITHM_VECTOR_H_

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define VECTOR(name, type)						\
struct name {								\
	type *data;							\
	size_t size, capacity;						\
	void (*delete)(type);						\
}

#define VECTOR_PROTOTYPE(name, type)					\
	VECTOR_PROTOTYPE_INTERNAL(name, type,)
#define VECTOR_PROTOTYPE_STATIC(name, type)				\
	VECTOR_PROTOTYPE_INTERNAL(name, type, static)
#define VECTOR_PROTOTYPE_INTERNAL(name, type, attr)			\
attr struct name * name##_new(void (*)(type));				\
attr void name##_delete(struct name *);					\
attr type *name##_data(struct name *);					\
attr size_t name##_size(struct name *);					\
attr size_t name##_capacity(struct name *);				\
attr size_t name##_max_size(void);					\
attr bool name##_empty(struct name *);					\
attr type name##_at(struct name *, size_t);				\
attr type name##_front(struct name *);					\
attr type name##_back(struct name *);					\
attr int name##_push_back(struct name *, type);				\
attr void name##_pop_back(struct name *);				\
attr void name##_clear(struct name *);					\
attr int name##_reserve(struct name *, size_t, type);			\
attr int name##_shrink_to_fit(struct name *);				\
attr int name##_insert(struct name *, size_t, type);			\
attr void name##_erase(struct name *, size_t);

#define VECTOR_GENERATE(name, type, default_capacity)			\
	VECTOR_GENERATE_INTERNAL(name, type, default_capacity,)
#define VECTOR_GENERATE_STATIC(name, type, default_capacity)		\
	VECTOR_GENERATE_INTERNAL(name, type, default_capacity, static)
#define VECTOR_GENERATE_INTERNAL(name, type, default_capacity, attr)	\
attr struct name *							\
name##_new(void (*delete)(type))					\
{									\
	struct name *v;							\
	v = malloc(sizeof(*v));						\
	if (v != NULL) {						\
		v->data = calloc(default_capacity, sizeof(*v->data));	\
		if (v->data != NULL) {					\
			v->size = 0;					\
			v->capacity = default_capacity;			\
			v->delete = delete;				\
			return v;					\
		}							\
		free(v);						\
	}								\
	return NULL;							\
}									\
attr void								\
name##_delete(struct name *v)						\
{									\
	size_t i;							\
	assert(v != NULL);						\
	if (v->delete != NULL) {					\
		for (i = 0; i < v->size; ++i)				\
			v->delete(v->data[i]);				\
	}								\
	free(v->data);							\
	free(v);							\
}									\
attr type *								\
name##_data(struct name *v)						\
{									\
	assert(v != NULL);						\
	return v->data;							\
}									\
attr size_t								\
name##_size(struct name *v)						\
{									\
	assert(v != NULL);						\
	return v->size;							\
}									\
attr size_t								\
name##_capacity(struct name *v)						\
{									\
	assert(v != NULL);						\
	return v->capacity;						\
}									\
attr size_t								\
name##_max_size(void)							\
{									\
	return SIZE_MAX / sizeof(type);					\
}									\
attr bool								\
name##_empty(struct name *v)						\
{									\
	assert(v != NULL);						\
	return v->size == 0;						\
}									\
attr type 								\
name##_at(struct name *v, size_t i)					\
{									\
	assert(v != NULL);						\
	assert(v->size > i);						\
	return v->data[i];						\
}									\
attr type 								\
name##_front(struct name *v)						\
{									\
	assert(v != NULL);						\
	assert(v->size > 0);						\
	return v->data[0];						\
}									\
attr type 								\
name##_back(struct name *v)						\
{									\
	assert(v != NULL);						\
	assert(v->size > 0);						\
	return v->data[v->size - 1];					\
}									\
attr int								\
name##_push_back(struct name *v, type e)				\
{									\
	size_t n;							\
	type *p;							\
	assert(v != NULL);						\
	assert(v->size <= v->capacity);					\
	if (v->size == v->capacity) {					\
		if ((SIZE_MAX / sizeof(type)) / 2 < v->capacity)	\
			return E2BIG;					\
		n = v->capacity * 2;					\
		p = realloc(v->data, n * sizeof(*v->data));		\
		if (p == NULL)						\
			return ENOMEM;					\
		v->data = p;						\
		v->capacity = n;					\
	} else {							\
		p = v->data;						\
	}								\
	p[v->size++] = e;						\
	return 0;							\
}									\
attr void								\
name##_pop_back(struct name *v)						\
{									\
	assert(v != NULL);						\
	assert(v->size > 0);						\
	--v->size;							\
	if (v->delete != NULL)						\
		v->delete(v->data[v->size]);				\
}									\
attr int								\
name##_reserve(struct name *v, size_t size, type e)			\
{									\
	size_t m, n;							\
	type *p;							\
	assert(v != NULL);						\
	assert(v->size <= v->capacity);					\
	if (size > v->size) {						\
		if (size > v->capacity) {				\
			m = size % v->capacity ? 1 : 0;			\
			m += size / v->capacity;			\
			if ((SIZE_MAX / sizeof(type)) / m < v->capacity)\
				return E2BIG;				\
			n = v->capacity * m;				\
			p = realloc(v->data, n * sizeof(*v->data));	\
			if (p == NULL)					\
				return ENOMEM;				\
			v->data = p;					\
			v->capacity = n;				\
		} else {						\
			p = v->data;					\
		}							\
		while (v->size < size)					\
			p[v->size++] = e;				\
	}								\
	return 0;							\
}									\
attr void								\
name##_clear(struct name *v)						\
{									\
	size_t i;							\
	assert(v != NULL);						\
	if (v->delete != NULL) {					\
		for (i = 0; i < v->size; ++i)				\
			v->delete(v->data[i]);				\
	}								\
	v->size = 0;							\
}									\
attr int								\
name##_shrink_to_fit(struct name *v)					\
{									\
	type *p;							\
	assert(v != NULL);						\
	p = realloc(v->data, v->size * sizeof(*v->data));		\
	if (p == NULL)							\
		return ENOMEM;						\
	v->data = p;							\
	v->capacity = v->size;						\
	return 0;							\
}									\
attr int								\
name##_insert(struct name *v, size_t i, type e)				\
{									\
	size_t n;							\
	type *p;							\
	assert(v != NULL);						\
	assert(v->size >= i);						\
	assert(v->size <= v->capacity);					\
	if (v->size == v->capacity) {					\
		if ((SIZE_MAX / sizeof(type)) / 2 < v->capacity)	\
			return E2BIG;					\
		n = v->capacity * 2;					\
		p = realloc(v->data, n * sizeof(*v->data));		\
		if (p == NULL)						\
			return ENOMEM;					\
		v->data = p;						\
		v->capacity = n;					\
	}								\
	for (n = v->size++; n > i; --n)					\
		v->data[n] = v->data[n - 1];				\
	v->data[n] = e;							\
	return 0;							\
}									\
attr void								\
name##_erase(struct name * v, size_t i)					\
{									\
	assert(v != NULL);						\
	assert(v->size > i);						\
	if (v->delete != NULL)						\
		v->delete(v->data[i]);					\
	--v->size;							\
	for (; i < v->size; ++i)					\
		v->data[i] = v->data[i + 1];				\
}

#define VECTOR_NEW(name, delete)		name##_new(delete)
#define VECTOR_DELETE(name, v)			name##_delete(v)
#define VECTOR_DATA(name, v)			name##_data(v)
#define VECTOR_SIZE(name, v)			name##_size(v)
#define VECTOR_CAPACITY(name, v)		name##_capacity(v)
#define VECTOR_MAX_SIZE(name)			name##_max_size()
#define VECTOR_EMPTY(name, v)			name##_empty(v)
#define VECTOR_AT(name, v, i)			name##_at(v, i)
#define VECTOR_FRONT(name, v)			name##_front(v)
#define VECTOR_BACK(name, v)			name##_back(v)
#define VECTOR_PUSH_BACK(name, v, e)		name##_push_back(v, e)
#define VECTOR_POP_BACK(name, v)		name##_pop_back(v)
#define VECTOR_RESERVE(name, v, size, e)	name##_reserve(v, size, e)
#define VECTOR_CLEAR(name, v)			name##_clear(v)
#define VECTOR_SHRINK_TO_FIT(name, v)		name##_shrink_to_fit(v)
#define VECTOR_INSERT(name, v, i, e)		name##_insert(v, i, e)
#define VECTOR_ERASE(name, v, i)		name##_erase(v, i)

#endif /*!ALGORITHM_VECTOR_H_*/
